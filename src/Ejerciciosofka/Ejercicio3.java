package Ejerciciosofka;
import java.util.Scanner;

 class Ejercicio3 {
    public static void main (String[] args){
        Scanner entrada = new  Scanner(System.in);

        System.out.println("Digite el Radio: ");
        double Radio =  Double.parseDouble(entrada.nextLine());
        double Area = Math.PI * Math.pow(Radio,2);

        System.out.println("El radio del circulo es: "+ Radio);
        System.out.print("El area del circulo es: "+ Area);

    }

}
