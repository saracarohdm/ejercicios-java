package Ejerciciosofka;
import java.util.Scanner;



public class Ejercicio15 {
        static int select = -1;
        static int num1 = 0, num2 = 0;
    public static void main (String []args){
        Scanner entrada= new Scanner(System.in);
        while(select != 0){

                try{
                    System.out.println("Elige opción:\n1.- NUEVO ACTOR" +
                            "\n2.- BUSCAR ACTOR \n" +
                            "3.- ELIMINAR ACTOR \n" +
                            "4.- MODIFICAR ACTOR \n" +
                            "5.- VER TODOS LOS ACTORES \n"+
                            "6.- VER PELICULAS DE LOS ACTORES \n"+
                            "7.- VER CATEGORIA DE LAS PELICULAS DE LOS ACTORES \n"+
                            "8.- Salir \n");

                    select = Integer.parseInt(entrada.nextLine());

                    switch(select){
                        case 1:
                            pideNumeros();
                            System.out.println(" Nuevo Actor");
                            break;
                        case 2:
                            pideNumeros();
                            System.out.println("Buscar Actor");
                            break;
                        case 3:
                            pideNumeros();
                            System.out.println("Elminar Actor");
                            break;
                        case 4:
                            pideNumeros();
                            System.out.println("Modificar Actor");
                            break;
                        case 5:
                            pideNumeros();
                            System.out.println("Ver Todos");
                            break;
                        case 6:
                            pideNumeros();
                            System.out.println("Ver Peliculas");
                            break;
                        case 7:
                            pideNumeros();
                            System.out.println("Ver Categoria");
                            break;
                        case 8:
                            pideNumeros();
                            System.out.println("Salir");
                            break;
                        default:
                            System.out.println("Incorrecto, digite un numero valido");
                            break;
                    }

                    System.out.println("\n");

                }catch(Exception e){
                    System.out.println("Error!");
                }
            }

        }

        public static void pideNumeros(){
            Scanner entrada= new Scanner(System.in);
            System.out.println("Introduce el dato");
            num1 = Integer.parseInt(entrada.nextLine());
            System.out.println("\n");
        }

    }

